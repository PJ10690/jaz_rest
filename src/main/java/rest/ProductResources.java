package rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import services.ProductService;
import domain.Category;
import domain.Comment;
import domain.Product;

@Path("/product")
@Stateless
public class ProductResources {
	private ProductService db = new ProductService();
	
	@PersistenceContext
	EntityManager em;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getAll(){
		return em.createNamedQuery("product.all", Product.class).getResultList();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response Add(Product product){
		em.persist(product);
		return Response.ok(product.getId()).build();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("id") int id){
		Product result = em.createNamedQuery("product.id", Product.class)
				.setParameter("productId", id)
				.getSingleResult();
		if(result == null){
			return Response.status(404).build();
		}
		return Response.ok(result).build();
	}
	
	@PUT
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("id") int id, Product p){
		Product result = em.createNamedQuery("product.id", Product.class)
				.setParameter("productId", id)
				.getSingleResult();
		
		if(result == null){
			return Response.status(404).build();
		}
		
		result.setName(p.getName());
		result.setPrice(p.getPrice());
		result.setCategory(p.getCategory());
		
		em.persist(result);
		return Response.ok().build();
	}
	
	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") int id){
		Product result = em.createNamedQuery("product.id", Product.class)
				.setParameter("productId", id)
				.getSingleResult();
		if(result == null){
			return Response.status(404).build();
		}
		em.remove(result);
		return Response.ok().build();
	}
	
	@GET
	@Path("/{id}/comments")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Comment> getComments(@PathParam("id") int id){
		Product result = em.createNamedQuery("product.id", Product.class)
				.setParameter("productId", id)
				.getSingleResult();	
		
		if(result == null){
			return null;
		}
		return result.getComments();
	}
	
	@POST
	@Path("/{id}/comments")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addComment(@PathParam("id") int id, Comment comment){
		Product result = em.createNamedQuery("product.id", Product.class)
				.setParameter("productId", id)
				.getSingleResult();		
		if(result == null){
			return Response.status(404).build();
		}
		result.getComments().add(comment);
		em.persist(comment);
		return Response.ok().build();
	}
	
	@DELETE
	@Path("/comment/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteComment(@PathParam("id") int id){
		Product result = em.createNamedQuery("comment.id", Product.class)
				.setParameter("commentId", id)
				.getSingleResult();
		if(result == null){
			return Response.status(404).build();
		}
		em.remove(result);
		return Response.ok().build();
	}
	
	@GET
	@Path("/category/{category}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getProductsByCategory(@PathParam("category") Category category){
		List<Product> result = em.createNamedQuery("product.category", Product.class)
				.setParameter("productCategory", category)
				.getResultList();
		if(result == null){
			return null;
		}
		return result;
	}
	
	@GET
	@Path("/name/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getProductsByName(@PathParam("name") String name){
		List<Product> result = em.createNamedQuery("product.name", Product.class)
				.setParameter("productName", "%" + name + "%")
				.getResultList();
		if(result == null){
			return null;
		}
		return result;
	}
	
	@GET
	@Path("/price/{min}/{max}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getProductsByPrice(@PathParam("min") float minPrice, @PathParam("max") float maxPrice){
		List<Product> result = em.createNamedQuery("product.price", Product.class)
				.setParameter("minPrice", minPrice)
				.setParameter("maxPrice", maxPrice)
				.getResultList();
		if(result == null){
			return null;
		}
		return result;
	}
}
